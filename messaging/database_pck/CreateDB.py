import sqlite3
from sqlite3 import Error


def create_connection(db_file):
    #Create Database Connection to db_file, if not exist it will be created
    #Returns connection objection or None
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    return conn


def create_table(conn, create_table_sql):
    # create sqlTable from second param
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def main():
    database = r"database.db"

    sql_create_session_table = """ CREATE TABLE IF NOT EXISTS session (
                                        session_id text UNIQUE,
                                        description text NOT NULL,
                                        time text
                                    ); """

    sql_create_message_table = """CREATE TABLE IF NOT EXISTS message (
                                    message_id integer PRIMARY KEY,
                                    session_id integer NOT NULL,
                                    player_id text,
                                    messageType text NOT NULL,
                                    time real,
                                    surface text,
                                    FOREIGN KEY (session_id) REFERENCES session(session_id)
                                );"""

    sql_create_gaze_table = """CREATE TABLE IF NOT EXISTS gaze (
                                    gaze_id integer PRIMARY KEY,
                                    message_id integer NOT NULL,
                                    time text,
                                    norm_pos_x real,
                                    norm_pos_y real,
                                    confidence real,
                                    on_screen INTEGER,
                                    areas text,
                                    FOREIGN KEY (message_id) REFERENCES message(message_id)
                                );"""
    sql_create_fixation_table = """CREATE TABLE IF NOT EXISTS fixation (
                                    fixation_id integer PRIMARY KEY,
                                    message_id integer NOT NULL,
                                    time text,
                                    norm_pos_x real,
                                    norm_pos_y real,
                                    screen_pos_x real,
                                    screen_pos_y real,
                                    confidence real,
                                    duration real,
                                    FOREIGN KEY (message_id) REFERENCES message(message_id)
                                );"""
    sql_create_gameobject_table = """CREATE TABLE IF NOT EXISTS gameobject (
                                    gameobject_id integer PRIMARY KEY,
                                    createjs_id integer NOT NULL,
                                    track_id integer NOT NULL,
                                    session_id text NOT NULL,
                                    time_pupil text,
                                    time_mtlg text,
                                    name text,
                                    pos_x real,
                                    pos_y real,
                                    parent text,
                                    FOREIGN KEY (session_id) REFERENCES session(session_id)
                                );"""
    # create a database connection
    conn = create_connection(database)

    # create tables
    if conn is not None:
        # create session table
        create_table(conn, sql_create_session_table)

        # create message table
        create_table(conn, sql_create_message_table)

        #create  gaze table
        create_table(conn, sql_create_gaze_table)

        #create  fixation table
        create_table(conn, sql_create_fixation_table)

        #create  gameobject table
        create_table(conn, sql_create_gameobject_table)
    else:
        print("Error! cannot create the database connection.")


if __name__ == '__main__':
    main()
