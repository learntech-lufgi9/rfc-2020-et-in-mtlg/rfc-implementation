# rfc-implementation
Implementation of a master-client program pair that can connect to local [`Pupil Core` eye-tracking software](https://docs.pupil-labs.com/core/) and collect surface tracking data from multiple devices.
The master uses surface definitions to transform eye-tracking coordinates to screen space and allows you to define a list of areas within a surface that you are interested in.
Output data will include information on which areas were hit within each surface.

Data obtained from the clients can be stored in a database for post-processing or may be forwarded to a [`socket.io`](https://socket.io/) webserver, which then acts as a message broker for other software interested in surface events.
A simple demo server can be found under `test/server.py`, next to a mock command line interface acting as a web client communicating with the master via the web socket (`js_client.py`).

The master can (and has to be) be configured and controlled with an extensive set of [commands](#commands) from either the command line or, once connected, the web socket interface.

## Requirements: 

The software requires `python` version 3.7 or later (tested against 3.7, 3.8).
To connect to `Pupil Core` eye-tracking hardware, programs from the `Pupil Core` software suite are necessary, which may be obtained from the [`PupilLabs` website](https://docs.pupil-labs.com/core/).
The Surfaces that are to be tracked need to be set up in the [`Surface Tracking` plugin](https://docs.pupil-labs.com/core/software/pupil-capture/#surface-tracking), which must be enabled.

Within `python`, we depend on the libraries
* [`pyzmq`](https://pyzmq.readthedocs.io/en/latest/) and [`pyre`](https://github.com/zeromq/pyre) for implementations of the [ZMQ](https://zeromq.org/) and [ZRE](https://rfc.zeromq.org/spec/36/) messaging and networking protocols used by `Pupil Core` software,
* [python-socketio](https://python-socketio.readthedocs.io/en/latest/index.html) for the [`socket.io` web server](https://socket.io/),
* `requests` and
* `msgpack` (as well as the built-in `json`) for (de-)serialization

Running the demo server additionally requires the `eventlet` package.
To install all dependencies, use the following command:

```bash
python -m pip install pyzmq https://github.com/zeromq/pyre/archive/master.zip python-socketio requests msgpack eventlet
```

## Usage
After cloning the project, first designate one device (which does not necessarily need to run eye-tracking software) to run the backend server and master node and initialize the database on this device:
```bash
cd <repository-folder>/messaging/database_pck && python CreateDB.py
```

On each eye-tracking device, connect the eye-tracking glasses and start the Pupil Core software.
Calibrate the glasses with the software. You may need to physically adjust the headset's eye cameras or world camera for optimal calibration. 
To track a screen, add a surface in the Surface Tracking plugin while looking at the screen with all april tags in the view. The MTLG eye-tracking module currently expects this surface to be called "surface" if it represents the game screen. If you intend to interface with the MTLG framework, this name can be set through the plugin after adding the surface.

On the master device, start the backend server:
```bash
cd <repository-folder>/backend && npm install && node server.js
```

Start the clients that connect to the Pupil Software on all eye-tracking devices with 
```bash
cd <repository-folder> && python messaging/client.py
```
and, on the master device, start the master with
```bash
cd <repository-folder> && python messaging/master.py
```
The master needs to know the location of the backend server.
To set this, run
```bash
pupil >> connect <IP address> <Port>
```
in the master's console.

On the master device, start your game using the MTLG eye-tracking module.
It will connect to the backend server automatically and initialize the master and the clients.

## Commands
The master accepts commands from both the command line interface and the web socket connection.
Depending on the source of a command, the result is either printed to the screen or sent as a response through the web socket.
Web socket responses are of the form `{"response": message, "cmd": original_command}`, where `original_command` is the command that triggered this response.

### Available Commands

| Command        | Function                                |
| -------------- | --------------------------------------- |
| `clients list` | Return the list of currently connected clients. |
| `time get` | Queries all clients for their current time (`Pupil Core` timestamp). Once an answer is received from all clients, returns a dictionary with entries of the form `client_id: client_time`. |
| `time set <timestamp>` | Instructs all clients to update their `Pupil Core` software's time to `timestamp`, which may be a floating point value. **Note:** at the time of writing there seems to be a bug in the `Pupil Core` software where setting the time can crash the `FixationDetector` plugin without any indication (the plugin is shown to be running in the `Pupil Core` UI). To remedy this, the `activate` command (see below) may be used to re-activate the plugin (this is done automatically for You when using the MTLG module). |
| `surface list` | Returns the list of known surfaces. |
| `surface add <name> [<x0> <y0> <x1> <y1>]` | Adds a new surface with the name `name`. If `x0` to `y1` are given, the screen space of the new surface is set to go from `(x0, y0)` (top left) to `(x1, y1)` (bottom right). |
| `surface remove <name>` | Removes all surfaces with the name `name`. |
| `screenspace get <name>` | Returns the screen space coordinates of the surface named `name`, if known. |
| `screenspace set <name> <x0> <y0> <x1> <y1>` | Sets the screen space coordinates of the surface named `name` to go from `(x0, y0)` (top left) to `(x1, y1)` (bottom right), if such a surface is known. |
| `area list <name>` | Returns the list of registered areas of the surface named `name`, if known. |
| `area add <surface_name> <area_name> <x0> <y0> <x1> <y1>` | Adds an area named `area_name` to the surface named `surface_name` which goes from `(x0, y0)` (top left) to `(x1, y1)` (bottom right), if such a surface is known. |
| `exclude gaze` and `include gaze` | Execute to either ex- or include (default is include) individual gaze data points from/in the surface tracking data that is logged and sent to the web server. |
| `subscribe` and `unsubscribe` | After `subscribe` is executed, all incoming surface tracking data is forwarded to the web server. `unsubscribe` turns off this behaviour. |
| `connect <address> <port>` | Initiates a connection attempt to `http://<addr>:<port>` to connect to the web server. |
| `confidence <threshold>` | Instructs clients to only forward data points with a confidence level above `threshold`. Confidence is expressed as a floating point value in the range of `0.0` (low) to `1.0` (high). See [Rate Limit, Thresholds and Filtering](#rate-limit-thresholds-and-filtering). |
| `interval <seconds>` | Instructs clients to only send updates at most every `seconds` seconds. See [Rate Limit, Thresholds and Filtering](#rate-limit-thresholds-and-filtering). |
| `activate <name>` | Instructs `Pupil Core` software to start the plugin `name`. |
| `exit`  | Quits the master program and notifies all clients to terminate as well.|
| `session set <session_id> <timestamp>` | set session_id and inserts session data into DB. |
| `session get` | returns session_id  |


## Rate Limit, Thresholds and Filtering
The `Pupil Core` software produces a large amount of events even for one eye-tracking device.
When working with multiple devices/clients, it may be desirable to reduce the amount of messages that are sent to and processed by the master program.

Client software will automatically filter out data points from `Pupil Core` surface tracking messages that do not lie on any known Surface.
If a message contains only such data points, the entire message will be filtered out.
Additionally, clients will filter out any data points with a confidence score below a certain threshold.
The confidence level ranges between `0.0` (low) and `1.0` (high), and the default threshold is `0.75`.
The threshold may be adjusted with the [`confidence` command](#commands).

In a situation in which a user program subscribes to the surface tracking data and does additional processing of its own, the volume of filtered data may still be to high to keep up with (or in order to do so the confidence threshold would need to be set unreasonably high).
For such cases, clients also contain a timer and will only send new messages after a set time interval has passed since sending the previous message.
By default, a new update is transmitted at most every `0.075` seconds.
This value may be changed with the [`interval` command](#commands).
