import time
import zmq
from zmq.utils.monitor import recv_monitor_message
from pyre import Pyre, zhelper
import uuid
import json
import msgpack

from typing import *

# constants for now, TODO change
IP = "127.0.0.1"
PORT = "50020"
TOPICS = ("surface",)
MTLG_GROUP = "MTLG"

# throughput and filtering settings
UPDATE_INTERVAL_S = 0.075
CONFIDENCE_THRESHOLD = 0.75

last_update = time.perf_counter()


def connect_blocking(socket: zmq.Socket, addr: str):
    monitor = socket.get_monitor_socket()
    socket.connect(addr)
    while True:
        status = recv_monitor_message(monitor)
        if status["event"] == zmq.EVENT_CONNECTED:
            break
        elif status["event"] == zmq.EVENT_CONNECT_DELAYED:
            pass
        else:
            raise Exception("ZMQ connection failed")
    socket.disable_monitor()


def recv_twopart(socket: zmq.Socket) -> Tuple[str, dict]:
    """Recv a message with topic, payload.

    Topic is a utf-8 encoded string. Returned as unicode object.
    Payload is a msgpack serialized dict. Returned as a python dict.

    Any addional message frames will be added as a list
    in the payload dict with key: '__raw_data__' .
    """
    topic = socket.recv_string()
    remaining_frames = recv_remaining_frames(socket)
    payload = deserialize_payload(*remaining_frames)
    return topic, payload


def recv_remaining_frames(socket: zmq.Socket):
    while socket.get(zmq.RCVMORE):
        yield socket.recv()


def deserialize_payload(payload_serialized: bytes, *extra_frames) -> dict:
    payload = msgpack.loads(payload_serialized)
    if extra_frames:
        payload["__raw_data__"] = extra_frames
    return payload


def filter_surface_message(message: dict) -> dict:
    def f(item):
        return item['confidence'] >= CONFIDENCE_THRESHOLD and item['on_surf']
    message['gaze_on_surfaces'] = list(filter(f, message['gaze_on_surfaces']))
    message['fixations_on_surfaces'] = list(filter(
        f, message['fixations_on_surfaces']))

    return message


if __name__ == '__main__':
    master = None
    master_joined = False

    group = Pyre(ctx=zmq.Context())
    group.join(MTLG_GROUP)
    group.start()

    # connect to pupil
    pupil_instance = zmq.Socket(zmq.Context(), zmq.REQ)
    pupil_instance.connect(f'tcp://{IP}:{PORT}')

    # Test connection get version string
    pupil_instance.send_string("v")
    v = pupil_instance.recv_string()
    print(f"Version: {v}")

    # Obtain IPC Backbone access (read)
    pupil_instance.send_string("SUB_PORT")
    SUB_PORT = pupil_instance.recv_string()

    subscriber = zmq.Socket(zmq.Context(), zmq.SUB)
    connect_blocking(subscriber, f'tcp://{IP}:{SUB_PORT}')
    for topic in TOPICS:
        subscriber.subscribe(topic)

    # loop on inputs from both the connection to master and the connection to pupil
    poller = zmq.Poller()
    poller.register(group.inbox, zmq.POLLIN)
    poller.register(subscriber, zmq.POLLIN)
    while(True):
        items = dict(poller.poll())
        if group.inbox in items and items[group.inbox] == zmq.POLLIN:
            # group is the connection between devices
            # thus, we listen for new members joining and commands from master
            msg = group.recv()
            msg_type = msg.pop(0).decode('utf-8')
            fromnode = uuid.UUID(bytes=msg.pop(0))
            msg_id = msg.pop(0).decode('utf-8')
            output = f"[{msg_type}] ({msg_id}) [node = {fromnode}]"
            if msg_type == "ENTER":
                headers = json.loads(msg.pop(0).decode('utf-8'))
                address = msg.pop(0).decode('utf-8')
                print(f"{output} discovered at '{address}'")
                for key in headers:
                    print(f"   {key} = {headers[key]}")
                    if key == "MASTER":
                        master = fromnode
            elif msg_type == "JOIN":
                group_name = msg.pop(0).decode('utf-8')
                print(f"{output} joined group '{group_name}'")
                if master is not None and group_name == MTLG_GROUP and fromnode == master:
                    # the master has joined the group
                    master_joined = True
            elif msg_type == "SHOUT":
                # handle instruction sent by master
                group_name = msg.pop(0).decode('utf-8')
                message = msg.pop(0).decode('utf-8')
                print(f"{output} to group '{group_name}': {message}")
                if message == "exit":
                    # if the command received is "exit", we need to shut down as well
                    break
                elif message.startswith("time"):
                    remaining_cmd = message[5:].strip()
                    if remaining_cmd == "get":
                        pupil_instance.send_string("t")
                        current_time = pupil_instance.recv_string()
                        if master_joined and master is not None:
                            group.whisper(master, current_time.encode('utf-8'))
                    elif remaining_cmd.startswith("set"):
                        pupil_instance.send_string(f"T {remaining_cmd[4:]}")
                        pupil_instance.recv_string()
                elif message.startswith("confidence"):
                    remaining_cmd = message[11:].strip()
                    try:
                        CONFIDENCE_THRESHOLD = float(remaining_cmd)
                    except:
                        pass
                elif message.startswith("interval"):
                    remaining_cmd = message[9:].strip()
                    try:
                        UPDATE_INTERVAL_S = float(remaining_cmd)
                    except:
                        pass
                elif message.startswith("activate"):
                    remaining_cmd = message[9:].strip()
                    cmd = {"subject": "start_plugin", "name": remaining_cmd}
                    pupil_instance.send_string(
                        "notify.start_plugin", flags=zmq.SNDMORE)
                    pupil_instance.send(msgpack.dumps(cmd, use_bin_type=True))
                    pupil_instance.recv_string()
        if subscriber in items and items[subscriber] == zmq.POLLIN:
            # subscriber is the connection to pupil
            # thus we forward messages to (master)
            (topic, message) = recv_twopart(subscriber)
            t = time.perf_counter()
            if t - last_update > UPDATE_INTERVAL_S:
                if not master_joined or master is None:
                    last_update = t
                else:
                    confident_hits = filter_surface_message(message)
                    # forward if the message contains a relevant event. don't update time if the
                    # event was empty, so we don't discard useful messages in the next
                    # UPDATE_INTERVAL_S time
                    if len(confident_hits['gaze_on_surfaces']) + len(confident_hits['fixations_on_surfaces']) > 0:
                        last_update = t
                        group.whisper(master, json.dumps(
                            confident_hits).encode('utf-8'))

    if subscriber is not None:
        for topic in TOPICS:
            subscriber.unsubscribe(topic)
        subscriber.disconnect(f'tcp://{IP}:{SUB_PORT}')
        subscriber.close()
    if pupil_instance is not None:
        pupil_instance.disconnect(f'tcp://{IP}:{PORT}')
        pupil_instance.close()
    group.stop()
