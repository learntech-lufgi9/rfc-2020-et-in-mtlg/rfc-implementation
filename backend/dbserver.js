var io = require('socket.io-client');
var sql = require('sqlite3');

var host = 'http://localhost'
var port = 5000;
console.log('connect to', host + ':' + port)
var session_id = null;
var socket = io.connect(host + ':' + port, { reconnect: true });
// Add a connect listener
socket.on('connect', function (socket) {
  console.log('Connected!');
});

socket.on('msg_py', function (data) {
  if (data.cmd != "exit" && data.event) {
    let messageJSON = data.event;
    // Insert message
    db_message = {}
    db_message["session_id"] = messageJSON["session_id"];
    if (messageJSON["session_id"] == null && session_id != null) {
      db_message["session_id"] = session_id
    }
    db_message["messageType"] = "Standard Message";
    db_message["time"] = messageJSON["time"] || null;
    db_message["surface"] = messageJSON["surface"] || null;
    db_message["player_id"] = messageJSON["player"] || null;
    db_message["gazes"] = messageJSON["gazes"] || null;
    db_message["fixations"] = messageJSON["fixations"] || null;

    var message_id = insert_message(db_message);
  }

});

socket.on('session_id', function (session) {
  session_id = session.session_id;
  insert_session(session);
});

socket.on('save_db', function (data) {
  var arr = [];
  arr.push(null);
  arr.push(data.createjs_id);
  arr.push(data.track_id);
  arr.push(data.session_id);
  arr.push(data.time_pupil);
  arr.push(data.time_mtlg);
  arr.push(data.name);
  arr.push(data.pos_x);
  arr.push(data.pos_y);
  arr.push(data.parent);
  db.run(`INSERT INTO gameobject VALUES(?,?,?,?,?,?,?,?,?,?)`, arr, function (err) {
    if (err) {
      return console.log(err.message);
    }
  });
});

var db = new sql.Database("../../rfc-implementation/messaging/database_pck/database.db", (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to database.');
});

function insert_message(message) {
  var arr = [message["session_id"], message["messageType"], message["time"], message['surface'], message["player_id"]];
  db.run(`INSERT INTO message (session_id, messageType, time, surface, player_id) VALUES (?,?,?,?,?)`, arr, function (err) {
    if (err) {
      return console.log(err.message);
    }
    // get the last insert id
    if (message.gazes && message.gazes.length > 0) {
      message.gazes.forEach((gaze, i) => {
        db_gaze = {};
        db_gaze["message_id"] = this.lastID;
        db_gaze["time"] = gaze["time"] || null;
        db_gaze["norm_pos_x"] = gaze["norm_pos"] && gaze["norm_pos"][0] ? gaze["norm_pos"][0] : null;
        db_gaze["norm_pos_y"] = gaze["norm_pos"] && gaze["norm_pos"][1] ? gaze["norm_pos"][1] : null;
        db_gaze["screen_pos_x"] = gaze["screen_pos"] && gaze["screen_pos"][0] ? gaze["screen_pos"][0] : null;
        db_gaze["screen_pos_y"] = gaze["screen_pos"] && gaze["screen_pos"][1] ? gaze["screen_pos"][1] : null;
        db_gaze["confidence"] = gaze["confidence"] || null;
		db_gaze["areas"] = gaze["areas"] || null;
        insert_gaze(db_gaze);
      });
    }

    if (message.fixations && message.fixations.length > 0) {
      message.fixations.forEach((fix, i) => {
        db_fix = {}
        db_fix["message_id"] = this.lastID;
        db_fix["time"] = fix["time"] || null;
        db_fix["norm_pos_x"] = fix["norm_pos"] && fix["norm_pos"][0] ? fix["norm_pos"][0] : null;
        db_fix["norm_pos_y"] = fix["norm_pos"] && fix["norm_pos"][1] ? fix["norm_pos"][1] : null;
        db_fix["screen_pos_x"] = fix["screen_pos"] && fix["screen_pos"][0] ? fix["screen_pos"][0] : null;
        db_fix["screen_pos_y"] = fix["screen_pos"] && fix["screen_pos"][1] ? fix["screen_pos"][1] : null;
        db_fix["confidence"] = fix["confidence"] || null;
        db_fix["on_screen"] = fix["on_screen"] || null;
        db_fix["duration"] = fix["duration"] || null;
        insert_fixation(db_fix);
      });
    }
  });
}

function insert_session(session) {
  var arr = [session["session_id"], session["description"], session["time"]];
  db.run(`INSERT INTO session (session_id, description, time) VALUES (?,?,?)`, arr, function (err) {
    if (err) {
      return console.log(err.message);
    }
  });
}

function insert_gaze(gaze) {
  var arr = [gaze["message_id"], gaze["time"], gaze["norm_pos_x"], gaze["norm_pos_y"], gaze["confidence"], gaze["areas"]];
  db.run(`INSERT INTO gaze (message_id, time, norm_pos_x, norm_pos_y, confidence, areas) VALUES (?,?,?,?,?,?)`, arr, function (err) {
    if (err) {
      return console.log(err.message);
    }
  });
}

function insert_fixation(fixation) {
  var arr = [fixation["message_id"], fixation["time"], fixation["norm_pos_x"], fixation["norm_pos_y"], fixation["screen_pos_x"], fixation["screen_pos_y"], fixation["confidence"], fixation["duration"]];
  db.run(`INSERT INTO fixation (message_id, time, norm_pos_x, norm_pos_y, screen_pos_x, screen_pos_y, confidence, duration) VALUES (?,?,?,?,?,?,?,?)`, arr, function (err) {
    if (err) {
      return console.log(err.message);
    }
    // get the last insert id
    //console.log(`Inserted fixation with id ${this.lastID}`, this);
  });
}
