import sqlite3
from sqlite3 import Error
from datetime import datetime


def create_connection(db_file):
    #Create Database Connection to db_file, if not exist it will be created
    #Returns connection objection or None
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    return conn


def insert_session(conn, session):
    #return last inserted session_id
    if(session["session_id"] == None):
        print("Session ID is None, could not insert")
        return 0
    data_tuple = (session["session_id"],session["description"], session["time"]) 
    sql = "INSERT INTO session (session_id, description, time) VALUES (?,?,?)"
    cur = conn.cursor()
    cur.execute(sql, data_tuple)
    conn.commit()
    return cur.lastrowid


def insert_message(conn, message):
    #return last inserted message_id
    data_tuple = (message["session_id"], message["messageType"], message["time"], message['surface'], message["player_id"]) 
    sql = "INSERT INTO message (session_id, messageType, time, surface, player_id) VALUES (?,?,?,?,?)"
    cur = conn.cursor()
    cur.execute(sql, data_tuple)
    conn.commit()
    return cur.lastrowid

def insert_gaze(conn, gaze):
    #return last inserted message_id
    data_tuple = (gaze["message_id"], gaze["time"], gaze["norm_pos_x"],gaze["norm_pos_y"], gaze["confidence"]) 
    sql = "INSERT INTO gaze (message_id, time, norm_pos_x, norm_pos_y, confidence) VALUES (?,?,?,?,?)"
    cur = conn.cursor()
    cur.execute(sql, data_tuple)
    conn.commit()
    return cur.lastrowid

def insert_fixation(conn, fixation):
    #return last inserted message_id
    data_tuple = (fixation["message_id"], fixation["time"], fixation["norm_pos_x"],fixation["norm_pos_y"],fixation["screen_pos_x"],fixation["screen_pos_y"], fixation["confidence"], fixation["duration"]) 
    sql = "INSERT INTO fixation (message_id, time, norm_pos_x, norm_pos_y, screen_pos_x, screen_pos_y, confidence, duration) VALUES (?,?,?,?,?,?,?,?)"
    cur = conn.cursor()
    cur.execute(sql, data_tuple)
    conn.commit()
    return cur.lastrowid

def update_session(conn, session,session_id):
    #return last inserted session_id
    data_tuple = (session["description"], session["begin_time"], session["end_time"], session_id) 
    sql = "UPDATE session set description = ?, begin_time = ?, end_time = ? where session_id = ?"
    cur = conn.cursor()
    cur.execute(sql, data_tuple)
    return cur.lastrowid


def update_message(conn, message,message_id):
    #return last inserted message_id
    data_tuple = (message["session_id"], message["messageType"], message["time"], message['surface'], message_id) 
    sql = "Update message set session_id = ?, messageType = ?, time = ?, surface = ? where message_id = ?"
    cur = conn.cursor()
    cur.execute(sql, data_tuple)
    return cur.lastrowid

def update_gaze(conn, gaze,gaze_id):
    #return last inserted message_id
    data_tuple = (gaze["message_id"], gaze["time"], gaze["norm_pos_x"],gaze["norm_pos_y"],gaze["screen_pos_x"],gaze["screen_pos_y"], gaze["confidence"],gaze_id) 
    sql = "Update gaze set message_id = ?, time = ?, norm_pos_x = ?, norm_pos_y = ?, screen_pos_x = ?, screen_pos_y = ?, confidence = ?, where gaze_id = ?"
    cur = conn.cursor()
    cur.execute(sql, data_tuple)
    return cur.lastrowid



def main():
    database = r"database.db"

    # create a database connection
    conn = create_connection(database)
    with conn:
        # create a new session
        session = {
                    "description":"This is the secound session",
                   "begin_time":83125.1961525,
                   "end_time":None
                   }
        
        session_id = insert_session(conn, session)
        print("Session with id "+str(session_id)+" has been inserted")
        
        # create a new message
        message = {
                    "session_id":session_id,
                    "messageType":"Test_Message",
                    "time":123455.20345,
                    "surface":"surface"
                    
                }
        message_id = insert_message(conn, message)
        gaze = {
                    "message_id":message_id,
                    "time":12345.20345,
                    "norm_pos_x": 0.39535441994667053,
                    "norm_pos_y": 0.4066673219203949,
                    "screen_pos_x":759.0804862976074,
                    "screen_pos_y":439.2007076740265,
                    "confidence":None,
                    "on_screen":True
                }
        gaze2 = {
                    "message_id":message_id,
                    "time":1111.20345,
                    "norm_pos_x": 0.39535441994667053,
                    "norm_pos_y": 0.4066673219203949,
                    "screen_pos_x":759.0804862976074,
                    "screen_pos_y":439.2007076740265,
                    "confidence":None,
                    "on_screen":True
                }
        gaze_id = insert_gaze(conn, gaze)
        update_gaze(conn,gaze2,gaze_id)        

#if __name__ == '__main__':
#    main()