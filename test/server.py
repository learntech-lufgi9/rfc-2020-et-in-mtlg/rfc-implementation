import socketio
import eventlet
eventlet.monkey_patch()

PORT = 5000

sio = socketio.Server()
app = socketio.WSGIApp(sio)


@sio.event
def connect(sid, environ):
    print('connect ', sid)


@sio.event
def msg_js(sid, data):
    sio.emit('msg_js', data)


@sio.event
def msg_py(sid, data):
    sio.emit('msg_py', data)


if __name__ == "__main__":
    eventlet.wsgi.server(eventlet.listen(('', PORT)), app)
