import socketio

PORT = 5000
running = True
sio = socketio.Client()


@sio.event
def connect():
    print('connection established')
    sio.emit('msg_js', {'message': "Hallo from js"})


@sio.event
def msg_py(data):
    global running
    print(f'py msg received with {data}\nJS Client >> ', end="")
    if "cmd" in data:
        cmd = data['cmd']
        if cmd == "exit":
            running = False
            sio.disconnect()


@sio.event
def disconnect():
    print('disconnected from server')


sio.connect(f'http://localhost:{PORT}')

while running:
    try:
        cmd = input("JS Client >> ")
        if sio.connected:
            sio.emit('msg_js', {'cmd': cmd})
        if cmd == "exit":
            sio.disconnect()
            break
    except (KeyboardInterrupt, SystemExit):
        break
