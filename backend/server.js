var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var port = 5000;

const { spawn } = require('child_process');
const db_server = spawn('node', ['dbserver.js']);

function handle_exit(_signal) {
	io.close();
	db_server.kill('SIGINT');
}

process.on('SIGKILL', handle_exit);
process.on('SIGTERM', handle_exit);
process.on('SIGINT', handle_exit);
process.on('SIGQUIT', handle_exit);

io.on('connection', function (socket) {
	console.log('new connection');
	socket.on('sendMessage', function (to, content) {
		console.log('sendMessage', to, content);
	});

	socket.on('initialize', function () {
		console.log('initialize', arguments);
		io.emit('log', "Hallo");
	});

	socket.on('msg_js', function (data) {
		console.log('msg_js', data);
		socket.broadcast.emit('msg_js', data);
	})

	socket.on('msg_py', function (data) {
		socket.broadcast.emit('msg_py', data);
	});

	socket.on('session_id', function (session) {
		socket.broadcast.emit('session_id', session);
	});

	socket.on('save_db', function (data) {
		socket.broadcast.emit('save_db', data);
	});
});

// Wait for connections
http.listen(port, function () {
	console.log('listening on *:' + port);
});
