
import zmq
from pyre import Pyre, zhelper
import socketio
import uuid
import json

from typing import *
from dataclasses import dataclass, field

import sqlite3
from sqlite3 import Error
from database_pck import DBActions as dba

import time
from threading import Thread

MTLG_GROUP = "MTLG"
MTLG_CONNECTION_ATTEMPTS = 5

# TYPES
Position = Tuple[float, float]
Area = Tuple[Position, Position]
Region = Tuple[str, Area]


@dataclass(eq=False)
class Surface:
    name: str
    screenspace: Position = ((0, 0), (1, 1))
    # [(region_name, ((x_0, y_0), (x_1, y_1)))] of rects
    regions: List[Region] = field(default_factory=list)

    def __eq__(self, value):
        return self.name.__eq__(value.name)

    def as_dict(self):
        return {
            "name": self.name,
            "screenspace": self.screenspace,
            "areas": self.regions
        }


# RUNTIME DATA
clients = []  # [uuid]
devices = {}  # {uuid: address}
surfaces: List[Surface] = []

running = True
group = None
communicate_connection = None
pending_cmd = None
exclude_gaze_data = False
mtlg_is_subscribed = False
session_id = None


def get_surface_by_name(surfaces: List[Surface], name: str) -> Union[Surface, None]:
    surface = None
    for s in surfaces:
        if s.name == name:
            surface = s
    return surface


# ============================================================================

def print_with_new_prompt(message):
    print(f"{message}\npupil >> ", end='')


def lerp_pos(position: Position, screenspace: Area) -> Position:
    x = position[0]
    # Pupil surface tracking uses bottom left corner as (0,0), but MTLG uses top left.
    # IMPORTANT: this requires that the given position's y coordinate be between 0 and 1
    y = 1 - position[1]
    screen_x0 = screenspace[0][0]
    screen_x1 = screenspace[1][0]
    screen_y0 = screenspace[0][1]
    screen_y1 = screenspace[1][1]
    delta_x = screen_x1 - screen_x0
    delta_y = screen_y1 - screen_y0
    return (screen_x0 + delta_x * x, screen_y0 + delta_y * y)


def region_contains(region: Region, x: float, y: float) -> bool:
    region_coords = region[1]
    contains_x = x > region_coords[0][0] and x < region_coords[1][0]
    contains_y = y > region_coords[0][1] and y < region_coords[1][1]
    return contains_x and contains_y


def translate(json_obj: Dict[str, Any], fromnode: str, surfaces: List[Surface]) -> Dict[str, Any]:
    global exclude_gaze_data

    result = {"player": fromnode}
    if(session_id):
        result['session_id'] = session_id
    if json_obj['topic'].startswith("surface"):
        time = json_obj['timestamp']
        result['time'] = time
        surface_name = json_obj['name']
        surface = get_surface_by_name(surfaces, surface_name)
        result['surface'] = surface_name
        result['areas'] = []

        gazes = json_obj['gaze_on_surfaces']
        result['gazes'] = []
        for gaze in gazes:
            local_time = gaze['timestamp']
            norm_pos = gaze['norm_pos']
            gaze_result = {'time': local_time,
                           'confidence': gaze['confidence'],  'areas': []}
            if surface:
                screen_pos = lerp_pos(norm_pos, surface.screenspace)
                gaze_result['screen_pos'] = screen_pos
                screen_x = screen_pos[0]
                screen_y = screen_pos[1]
                for region in surface.regions:
                    if region_contains(region, screen_x, screen_y):
                        region_name = region[0]
                        gaze_result['areas'].append(region_name)
                        if region_name not in result['areas']:
                            result['areas'].append(region_name)
            else:
                # include norm_pos as backup
                gaze_result['norm_pos'] = norm_pos

            if not exclude_gaze_data:
                # this happens down here so we still do the area calculation for the event
                result['gazes'].append(gaze_result)

        fixations = json_obj['fixations_on_surfaces']
        result['fixations'] = []
        for fixation in fixations:
            local_time = fixation['timestamp']
            norm_pos = fixation['norm_pos']
            fixation_result = {
                'time': local_time, 'confidence': fixation['confidence'], 'duration': fixation['duration'], 'areas': []}
            if surface:
                screen_pos = lerp_pos(norm_pos, surface.screenspace)
                fixation_result['screen_pos'] = screen_pos
                screen_x = screen_pos[0]
                screen_y = screen_pos[1]
                areas = []
                for region in surface.regions:
                    if region_contains(region, screen_x, screen_y):
                        region_name = region[0]
                        fixation_result['areas'].append(region_name)
                        if region_name not in result['areas']:
                            result['areas'].append(region_name)
            else:
                # include norm_pos as backup
                fixation_result['norm_pos'] = norm_pos

            result['fixations'].append(fixation_result)

    return result


def handle_cmd(cmd: str, target: str) -> str:
    global surfaces, clients, devices, pending_cmd, running, exclude_gaze_data, mtlg_is_subscribed, session_id

    if target == "js":
        def answer(m): return sio.emit('msg_py', {"response": m, "cmd": cmd})
    else:
        def answer(m): return print_with_new_prompt(m)

    if cmd == "exit":
        # inform clients of termination and exit
        if MTLG_GROUP in group.peer_groups():
            group.shout(MTLG_GROUP, cmd.encode('utf-8'))
        running = False
        return "exit"
    elif cmd == "clients list":
        answer([str(client) for client in clients])
    elif cmd.startswith("confidence") or cmd.startswith("interval"):
        # forward client settings
        group.shout(MTLG_GROUP, cmd.encode('utf-8'))
    elif cmd.startswith("time"):
        if cmd[5:].startswith("get"):
            pending_cmd = {"cmd": cmd, "target": target,
                           "responses": {}, "pending_clients": clients[:]}
        group.shout(MTLG_GROUP, cmd.encode('utf-8'))
    elif cmd.startswith("activate"):
        group.shout(MTLG_GROUP, cmd.encode('utf-8'))
    elif cmd.startswith("surface"):
        remaining_cmd = cmd[8:].strip()
        if remaining_cmd == "list":
            answer([surface.as_dict() for surface in surfaces])
        else:
            parts = remaining_cmd.split()
            if len(parts) < 2:
                answer(
                    "Usage: surface add <name> [<x0> <y0> <x1> <y1>] or surface remove <name>")
            elif parts[0].strip() == "add":
                if len(parts) != 2 and len(parts) != 6:
                    answer(
                        "Usage: surface add <name> [<x0> <y0> <x1> <y1>] or surface remove <name>")
                else:
                    name = parts[1].strip()
                    surface = Surface(name)
                    if len(parts) == 6:
                        surface.screenspace = ((float(parts[2]), float(
                            parts[3])), (float(parts[4]), float(parts[5])))
                    surfaces.append(surface)
            elif parts[0].strip() == "remove":
                name = parts[1].strip()
                surfaces = list(filter(lambda s: s.name != name, surfaces))
            else:
                answer(
                    "Usage: surface add <name> [<x0> <y0> <x1> <y1>] or surface remove <name>")
    elif cmd.startswith("screenspace"):
        remaining_cmd = cmd[12:].strip()
        if remaining_cmd.startswith("get"):
            parts = remaining_cmd.split()
            if len(parts) != 2:
                answer("Usage: screenspace get <name>")
            else:
                name = parts[1].strip()
                surface = get_surface_by_name(surfaces, name)
                if not surface:
                    answer(f"Unknown surface '{name}'")
                else:
                    answer(surface.screenspace)
        elif remaining_cmd.startswith("set"):
            parts = remaining_cmd.split()
            if len(parts) != 6:
                answer("Usage: screenspace set <name> <x0> <y0> <x1> <y1>")
            else:
                name = parts[1].strip()
                surface = get_surface_by_name(surfaces, name)
                if not surface:
                    answer(f"Unknown surface '{name}'")
                else:
                    coords = parts[2:]
                    screenspace = ((float(coords[0]), float(
                        coords[1])), (float(coords[2]), float(coords[3])))
                    surface.screenspace = screenspace
        else:
            answer(f"Unknown command: {cmd}")
    elif cmd.startswith("area"):
        remaining_cmd = cmd[5:]
        if remaining_cmd.startswith("list"):
            parts = remaining_cmd.split()
            if len(parts) != 2:
                answer("Usage: area list <name>")
            else:
                name = parts[1].strip()
                surface = get_surface_by_name(surfaces, name)
                if not surface:
                    answer(f"Unknown surface '{name}'")
                else:
                    answer(surface.regions)
        elif remaining_cmd.startswith("add"):
            parts = remaining_cmd.split()
            if len(parts) != 7:
                answer(
                    "Usage: area add <surface_name> <area_name> <x0> <y0> <x1> <y1>")
            else:
                surface_name = parts[1].strip()
                area_name = parts[2].strip()
                surface = get_surface_by_name(surfaces, surface_name)
                if not surface:
                    answer(f"Unknown surface '{surface_name}'")
                else:
                    coords = parts[3:]
                    region = ((float(coords[0]), float(
                        coords[1])), (float(coords[2]), float(coords[3])))
                    surface.regions.append((area_name, region))
        else:
            answer(f"Unknown command: {cmd}")
    elif cmd.startswith("exclude gaze"):
        exclude_gaze_data = True
    elif cmd.startswith("include gaze"):
        exclude_gaze_data = False
    elif cmd == "subscribe":
        mtlg_is_subscribed = True
    elif cmd == "unsubscribe":
        mtlg_is_subscribed = False
    elif cmd.startswith("connect"):
        if not sio.connected:
            remaining_cmd = cmd[8:].strip()
            parts = remaining_cmd.split()
            if len(parts) != 2:
                print_with_new_prompt("Usage: connect <address> <port>")
            else:
                addr = parts[0].strip()
                port = parts[1].strip()
                url = f'http://{addr}:{port}'
                print(
                    f"pupil >> Connecting to the MTLG connection server at '{url}'.")
                for _ in range(MTLG_CONNECTION_ATTEMPTS):
                    try:
                        sio.connect(url)
                        break
                    except:
                        print_with_new_prompt(
                            "Connection attempt failed. Retrying...")
                else:  # no break - we are not connected
                    raise ConnectionError(
                        "Failed to connect to the MTLG connection server.")

                print_with_new_prompt("Connected.")
    elif cmd.startswith("session"):
        remaining_cmd = cmd[8:].strip()
        if remaining_cmd.startswith("set"):
            timeStamp = None
            tempData = remaining_cmd[4:].strip().split()
            session_id = tempData[0]
            timeStamp = tempData[1]
            # db = dba.create_connection("database_pck\\database.db")
            # db_message = {}
            # db_message["session_id"] = session_id
            # db_message["description"] = "Test Session"
            # db_message["time"] = timeStamp
            # row = dba.insert_session(db, db_message)
        elif remaining_cmd.startswith("get"):
            answer(str(session_id))
    return "noop"


sio = socketio.Client()
@sio.event
def msg_js(data):
    print_with_new_prompt(f'js msg received with {data}')
    if "cmd" in data:
        cmd = data['cmd']
        handle_cmd(cmd.strip(), "js")


def communicate(ctx, pipe):
    global surfaces, group, pending_cmd, mtlg_is_subscribed, running, communicate_connection

    group = Pyre(ctx=ctx)
    # indicate in the header that we are the master
    group.set_header("MASTER", "true")
    group.join(MTLG_GROUP)
    group.start()

    pipe.send('ready'.encode('utf-8'))

    poller = zmq.Poller()
    poller.register(pipe, zmq.POLLIN)
    poller.register(group.inbox, zmq.POLLIN)
    while running:
        items = dict(poller.poll())
        if pipe in items and items[pipe] == zmq.POLLIN:
            # pipe is the connection to the main thread, which handles cli
            message = pipe.recv().decode('utf-8')
            if handle_cmd(message.strip(), "cli") == "exit":
                break

        if group.inbox in items and items[group.inbox] == zmq.POLLIN:
            # group is the connection between devices
            # thus, we listen for new members joining and messages from clients
            msg = group.recv()
            msg_type = msg.pop(0).decode('utf-8')
            fromnode = uuid.UUID(bytes=msg.pop(0))
            msg_id = msg.pop(0).decode('utf-8')
            output = f"[{msg_type}] ({msg_id}) [node = {fromnode}]"
            if msg_type == "ENTER":
                headers = json.loads(msg.pop(0).decode('utf-8'))
                address = msg.pop(0).decode('utf-8')
                devices[fromnode] = address
            elif msg_type == "JOIN":
                group_name = msg.pop(0).decode('utf-8')
                if group_name == MTLG_GROUP:
                    clients.append(fromnode)
                    print_with_new_prompt(f"Client joined group: {fromnode}")
            elif msg_type == "WHISPER":
                message = msg.pop(0).decode('utf-8')
                try:
                    # see if the message is only a number, in which case it is a response to `time get`
                    time = float(message)
                    if pending_cmd is not None:
                        # `fromnode` is a UUID, thus the string conversion
                        pending_cmd['responses'][str(fromnode)] = time
                        pending_cmd['pending_clients'].remove(fromnode)
                        if not pending_cmd['pending_clients']:
                            # we have received an anwer from all clients we were waiting for
                            if pending_cmd["target"] == "js":
                                sio.emit(
                                    'msg_py', {"response": pending_cmd['responses'], "cmd": pending_cmd["cmd"]})
                            elif pending_cmd["target"] == "cli":
                                print_with_new_prompt(
                                    f"Command resolved: result of \"{pending_cmd['cmd']}\" is: {pending_cmd['responses']}")
                            # reset waiting for responses
                            # TODO: this is still racy in case two time requests are sent simultaneously
                            pending_cmd = None
                except ValueError:
                    # the message is a full message (event)
                    obj = json.loads(message)
                    processed = translate(obj, str(fromnode), surfaces)
                    if mtlg_is_subscribed:
                        sio.emit('msg_py', {"event": processed})
                # print_with_new_prompt(message)
    communicate_connection = None
    if sio.connected:
        sio.emit('msg_py', {'cmd': 'exit'})
        sio.disconnect()
    group.stop()


if __name__ == '__main__':
    communicate_connection = zhelper.zthread_fork(
        zmq.Context(), communicate)
    # wait for thread to be ready
    communicate_connection.recv()

    while running:
        try:
            cmd = input("pupil >> ")
            if communicate_connection is not None:
                communicate_connection.send(cmd.encode('utf-8'))
        except (KeyboardInterrupt, SystemExit):
            break
