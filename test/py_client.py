import socketio

PORT = 5000

sio = socketio.Client()


@sio.event
def connect():
    print('connection established')
    sio.emit('msg_py', {'message': "Hallo from py"})


@sio.event
def msg_js(data):
    print('js msg received with ', data)
    if "cmd" in data:
        cmd = data['cmd']
        if cmd == "exit":
            sio.disconnect()


@sio.event
def disconnect():
    print('disconnected from server')


sio.connect(f'http://localhost:{PORT}')
sio.wait()
